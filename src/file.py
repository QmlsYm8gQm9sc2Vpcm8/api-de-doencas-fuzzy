from src.config.settings import LOCAL_ARQUIVOS_DE_ENTRADA, LOCAL_ARQUIVOS_DE_SAIDA, \
    ARQUIVO_DE_ENTRADA, ARQUIVO_DE_SAIDA

class File:

    def read_file(self):
        arquivo = open(LOCAL_ARQUIVOS_DE_ENTRADA + ARQUIVO_DE_ENTRADA, 'r')
        respostas = list()
        for linha in arquivo:
            print(linha)
            respostas.append(linha)
        return respostas

    def write_file(self, resultado):
        with open(LOCAL_ARQUIVOS_DE_SAIDA + ARQUIVO_DE_SAIDA, 'w') as arquivo:
            arquivo.write(str(resultado))