class Comparador:

    def __init__(self):
        self.contador_gripe_comum = [0]
        self.contador_h1n1 = [0]
        self.contador_h3n2 = [0]
        self.contador_influenza_a = [0]
        self.contador_influenza_b = [0]
        self.contador_influenza_c = [0]
        self.contador_total = 0

    def verificador_de_resposta(self, resposta, indice, contador):
         if int(resposta[indice]) == 1:
             contador[0] += 1


    def is_gripe_comum(self, resposta_usuario):
       self.verificador_de_resposta(resposta_usuario, 0, self.contador_gripe_comum)
       self.verificador_de_resposta(resposta_usuario, 1, self.contador_gripe_comum)
       self.verificador_de_resposta(resposta_usuario, 2, self.contador_gripe_comum)
       self.verificador_de_resposta(resposta_usuario, 3, self.contador_gripe_comum)
       self.verificador_de_resposta(resposta_usuario, 4, self.contador_gripe_comum)
       self.verificador_de_resposta(resposta_usuario, 5, self.contador_gripe_comum)
       self.verificador_de_resposta(resposta_usuario, 6, self.contador_gripe_comum)

    def is_h1n1(self, resposta_usuario):
        self.verificador_de_resposta(resposta_usuario, 0, self.contador_h1n1)
        self.verificador_de_resposta(resposta_usuario, 1, self.contador_h1n1)
        self.verificador_de_resposta(resposta_usuario, 18, self.contador_h1n1)
        self.verificador_de_resposta(resposta_usuario, 2, self.contador_h1n1)
        self.verificador_de_resposta(resposta_usuario, 3, self.contador_h1n1)
        self.verificador_de_resposta(resposta_usuario, 4, self.contador_h1n1)
        self.verificador_de_resposta(resposta_usuario, 7, self.contador_h1n1)
        self.verificador_de_resposta(resposta_usuario, 8, self.contador_h1n1)
        self.verificador_de_resposta(resposta_usuario, 9, self.contador_h1n1)
        self.verificador_de_resposta(resposta_usuario, 10, self.contador_h1n1)

    def is_h3n2(self, resposta_usuario):
        self.verificador_de_resposta(resposta_usuario, 0, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 1, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 18, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 19, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 11, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 4, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 5, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 12, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 9, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 10, self.contador_h3n2)
        self.verificador_de_resposta(resposta_usuario, 13, self.contador_h3n2)

    def is_influenza_a(self, resposta_usuario):
        self.verificador_de_resposta(resposta_usuario, 0, self.contador_influenza_a)
        self.verificador_de_resposta(resposta_usuario, 1, self.contador_influenza_a)
        self.verificador_de_resposta(resposta_usuario, 2, self.contador_influenza_a)
        self.verificador_de_resposta(resposta_usuario, 18, self.contador_influenza_a)
        self.verificador_de_resposta(resposta_usuario, 14, self.contador_influenza_a)
        self.verificador_de_resposta(resposta_usuario, 4, self.contador_influenza_a)
        self.verificador_de_resposta(resposta_usuario, 7, self.contador_influenza_a)
        self.verificador_de_resposta(resposta_usuario, 6, self.contador_influenza_a)
        self.verificador_de_resposta(resposta_usuario, 15, self.contador_influenza_a)

    def is_influenza_b(self, resposta_usuario):
        self.verificador_de_resposta(resposta_usuario, 0, self.contador_influenza_b)
        self.verificador_de_resposta(resposta_usuario, 1, self.contador_influenza_b)
        self.verificador_de_resposta(resposta_usuario, 2, self.contador_influenza_b)
        self.verificador_de_resposta(resposta_usuario, 18, self.contador_influenza_b)
        self.verificador_de_resposta(resposta_usuario, 14, self.contador_influenza_b)
        self.verificador_de_resposta(resposta_usuario, 4, self.contador_influenza_b)
        self.verificador_de_resposta(resposta_usuario, 7, self.contador_influenza_b)
        self.verificador_de_resposta(resposta_usuario, 6, self.contador_influenza_b)
        self.verificador_de_resposta(resposta_usuario, 15, self.contador_influenza_b)
        self.verificador_de_resposta(resposta_usuario, 10, self.contador_influenza_b)

    def is_influenza_c(self, resposta_usuario):
        self.verificador_de_resposta(resposta_usuario, 16, self.contador_influenza_c)
        self.verificador_de_resposta(resposta_usuario, 2, self.contador_influenza_c)
        self.verificador_de_resposta(resposta_usuario, 4, self.contador_influenza_c)
        self.verificador_de_resposta(resposta_usuario, 17, self.contador_influenza_c)

    def valor_total(self, resposta_usuario):
        for resposta in resposta_usuario:
            if int(resposta) == 1:
                self.contador_total += 1

if __name__ == '__main__':
    p = Comparador()
    p.is_gripe_comum([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
    p.is_h1n1([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
    p.is_h3n2([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
    p.is_influenza_a([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
    p.is_influenza_b([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
    p.is_influenza_c([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
    print(int(p.contador_gripe_comum[0]))
    print(int(p.contador_h1n1[0]))
    print(int(p.contador_h3n2[0]))
    print(int(p.contador_influenza_a[0]))
    print(int(p.contador_influenza_b[0]))
    print(int(p.contador_influenza_c[0]))
    p.valor_total([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
    print(int(p.contador_total))
