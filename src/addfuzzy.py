from src.comparador import Comparador
from src.fuzzyficacao import Fuzzy
from src.resultado_doenca import ResultadoDoenca
from src.file import File

class AdDFuzzy:

    def __init__(self, respostas):
        comparador = Comparador()
        fuzzy = Fuzzy()
        resultado_final = ResultadoDoenca()

        comparador.is_h1n1(list(respostas))
        comparador.is_gripe_comum(list(respostas))
        comparador.is_h3n2(list(respostas))
        comparador.is_influenza_a(list(respostas))
        comparador.is_influenza_b(list(respostas))
        comparador.is_influenza_c(list(respostas))

        resultado_final.set_porcentagem_gripe_comum(fuzzy.defuzzyficacao(
            fuzzy.fuzzyficacao(
                fuzzy.calculo_de_entrada(
                    comparador.contador_gripe_comum[0],
                    "Gripe Comum"
                )
            )
        ))

        resultado_final.set_porcentagem_h1n1(fuzzy.defuzzyficacao(
            fuzzy.fuzzyficacao(
                fuzzy.calculo_de_entrada(
                    comparador.contador_h1n1[0],
                    "H1N1"
                )
            )
        ))

        resultado_final.set_porcentagem_h3n2(fuzzy.defuzzyficacao(
            fuzzy.fuzzyficacao(
                fuzzy.calculo_de_entrada(
                    comparador.contador_h3n2[0],
                    "H3N2"
                )
            )
        ))

        resultado_final.set_porcentagem_influenza_a(fuzzy.defuzzyficacao(
            fuzzy.fuzzyficacao(
                fuzzy.calculo_de_entrada(
                    comparador.contador_influenza_a[0],
                    "Influenza A"
                )
            )
        ))

        resultado_final.set_porcentagem_influenza_b(fuzzy.defuzzyficacao(
            fuzzy.fuzzyficacao(
                fuzzy.calculo_de_entrada(
                    comparador.contador_influenza_b[0],
                    "Influenza B"
                )
            )
        ))

        resultado_final.set_porcentagem_influenza_c(fuzzy.defuzzyficacao(
            fuzzy.fuzzyficacao(
                fuzzy.calculo_de_entrada(
                    comparador.contador_influenza_c[0],
                    "Influenza C"
                )
            )
        ))

        self.resultado = resultado_final.comparador_de_porcentagem()

    def resultado_final(self):
        return self.resultado