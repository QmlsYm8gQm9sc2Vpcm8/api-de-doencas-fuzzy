import os
from dotenv import load_dotenv

load_dotenv()

LOCAL_ARQUIVOS_DE_ENTRADA = str(os.getenv("LOCAL_ARQUIVOS_DE_ENTRADA"))
LOCAL_ARQUIVOS_DE_SAIDA = str(os.getenv("LOCAL_ARQUIVOS_DE_SAIDA"))
ARQUIVO_DE_ENTRADA = str(os.getenv("ARQUIVO_DE_ENTRADA"))
ARQUIVO_DE_SAIDA = str(os.getenv("ARQUIVO_DE_SAIDA"))
URL_BOT = str(os.getenv("URL_BOT"))

VALOR_DEFUZZYFICACAO = int(os.getenv("DEFUZZYFICACAO"))
VALOR_IDEAL = int(os.getenv("VALOR_IDEAL"))
VALOR_MINIMO = int(os.getenv("VALOR_MINIMO"))
VALOR_MAXIMO = int(os.getenv("VALOR_MAXIMO"))