class ResultadoDoenca:

    def __init__(self):
        self.__porcentagem_gripe_comum = None
        self.__porcentagem_h1n1 = None
        self.__porcentagem_h3n2 = None
        self.__porcentagem_influenza_a = None
        self.__porcentagem_influenza_b = None
        self.__porcentagem_influenza_c = None

    def set_porcentagem_influenza_a(self, porcentagem_influenza_a):
        self.__porcentagem_influenza_a = porcentagem_influenza_a

    def get_porcentagem_influenza_a(self):
        return self.__porcentagem_influenza_a

    def set_porcentagem_influenza_b(self, porcentagem_influenza_b):
        self.__porcentagem_influenza_b = porcentagem_influenza_b

    def get_porcentagem_influenza_b(self):
        return self.__porcentagem_influenza_b

    def set_porcentagem_influenza_c(self, porcentagem_influenza_c):
        self.__porcentagem_influenza_c = porcentagem_influenza_c

    def get_porcentagem_influenza_c(self):
        return self.__porcentagem_influenza_c

    def set_porcentagem_h1n1(self, porcentagem_h1n1):
        self.__porcentagem_h1n1 = porcentagem_h1n1

    def get_porcentagem_h1n1(self):
        return self.__porcentagem_h1n1

    def set_porcentagem_h3n2(self, porcentagem_h3n2):
        self.__porcentagem_h3n2 = porcentagem_h3n2

    def get_porcentagem_h3n2(self):
        return self.__porcentagem_h3n2

    def set_porcentagem_gripe_comum(self, porcentagem_gripe_comum):
        self.__porcentagem_gripe_comum = porcentagem_gripe_comum

    def get_porcentagem_gripe_comum(self):
        return self.__porcentagem_gripe_comum

    def comparador_de_porcentagem(self):
        resultado = list()
        resultado.append(self.get_porcentagem_gripe_comum())
        resultado.append(self.get_porcentagem_h1n1())
        resultado.append(self.get_porcentagem_h3n2())
        resultado.append(self.get_porcentagem_influenza_a())
        resultado.append(self.get_porcentagem_influenza_b())
        resultado.append(self.get_porcentagem_influenza_c())

        maior_porcentagem = max(resultado)

        if maior_porcentagem == self.get_porcentagem_gripe_comum():
            return "Gripe Comum", max(resultado)
        if maior_porcentagem == self.get_porcentagem_h1n1():
            return "Gripe H1N1", max(resultado)
        if maior_porcentagem == self.get_porcentagem_h3n2():
            return "Gripe H3N2", max(resultado)
        if maior_porcentagem == self.get_porcentagem_influenza_a():
            return "Influenza A", max(resultado)
        if maior_porcentagem == self.get_porcentagem_influenza_b():
            return "Influenza B", max(resultado)
        if maior_porcentagem == self.get_porcentagem_influenza_c():
            return "Influenza C", max(resultado)
