from src.config.settings import VALOR_DEFUZZYFICACAO, VALOR_IDEAL, VALOR_MAXIMO, VALOR_MINIMO
class Fuzzy:

    def __init__(self):

        self.total_de_elementos = {
            "Gripe Comum": 7,
            "H1N1": 10,
            "H3N2": 11,
            "Influenza A": 9,
            "Influenza B": 10,
            "Influenza C": 4
        }

        self.__porcentagem_gripe_comum = None
        self.__porcentagem_h1n1 = None
        self.__porcentagem_h3n2 = None
        self.__porcentagem_influenza_a = None
        self.__porcentagem_influenza_b = None
        self.__porcentagem_influenza_c = None


    def calculo_de_entrada(self, valor_de_entrada, index):
        return (int(valor_de_entrada) / self.total_de_elementos[index]) / 2

    def fuzzyficacao(self, calculo_de_entrada):
        #x = float(input("O Valor de X é a resposta da função x/nº de itens por doença/2: "))
        lado_a = ((float(calculo_de_entrada) - VALOR_MINIMO) / (VALOR_IDEAL - VALOR_MINIMO))
        lado_b = ((VALOR_MAXIMO - float(calculo_de_entrada)) / (VALOR_MAXIMO - VALOR_IDEAL))

        if (lado_a < lado_b):
            return lado_a
        else:
            return lado_b

    def defuzzyficacao(self, resultado):
        resultado *= VALOR_DEFUZZYFICACAO
        porcentagem = resultado * VALOR_DEFUZZYFICACAO
        return porcentagem

if __name__ == '__main__':
    obj = Fuzzy()
    print(obj.defuzzyficacao(obj.fuzzyficacao(obj.calculo_de_entrada(3, "Influenza C"))))
