from src.addfuzzy import AdDFuzzy, File

def perguntas(texto):
    while(True):
        resposta = input(texto)
        if resposta == '0'or resposta == '1':
            break
    return resposta

def main():
    respostas = list()

    tipo_de_entrada = perguntas("Selecione o Tipo de Entrada: (1 - Arquivo / 0 - Terminal): ")
    if tipo_de_entrada == '0':
        print("\nQuais os Sintomas que está sentindo? (1 - Sim / 0 - Não)")
        respostas.append(perguntas("Febre Alta: "))
        respostas.append(perguntas("Dor de Garganta e Dores no Corpo: "))
        respostas.append(perguntas("Tosse: "))
        respostas.append(perguntas("Congestão Nasal: "))
        respostas.append(perguntas("Coriza: "))
        respostas.append(perguntas("Fadiga: "))
        respostas.append(perguntas("Calafrios: "))
        respostas.append(perguntas("Insuficiência Respiratória: "))
        respostas.append(perguntas("Falta de Ar: "))
        respostas.append(perguntas("Nausea e Vômito: "))
        respostas.append(perguntas("Diarréia: "))
        respostas.append(perguntas("Vermelhidão nos Olhos: "))
        respostas.append(perguntas("Fraqueza: "))
        respostas.append(perguntas("Rouquidão: "))
        respostas.append(perguntas("Dores nas Articulações: "))
        respostas.append(perguntas("Hipóxia: "))
        respostas.append(perguntas("Irritação nos Olhos: "))
        respostas.append(perguntas("Cansaço: "))
        respostas.append(perguntas("Dor de Cabeça: "))
        respostas.append(perguntas("Tosse é Seca: \n"))

    elif tipo_de_entrada == '1':
        respostas = File().read_file()

    tipo_de_saida = perguntas("Selecione o Tipo de Saida: (1 - Arquivo / 0 - Tela): ")

    if tipo_de_saida == '0':
        nome, porcentagem = AdDFuzzy(respostas).resultado_final()
        print("\n" + nome + ": " + str(porcentagem) + '%')
    elif tipo_de_saida == '1':
        nome, porcentagem = AdDFuzzy(respostas).resultado_final()
        File().write_file(nome + ": " + str(porcentagem) + '%')

if __name__== "__main__":
    main()

